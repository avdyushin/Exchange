Demo app using [Swift 3](https://swift.org/), so need to use [Xcode 8](https://developer.apple.com/xcode/) beta 2 to build the project.

![image](converter@2x.png)
