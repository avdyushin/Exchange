//
//  ViewController.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 06.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let user = User()
    
    var dataProvider: DataProvider? = Ecb()
    
    lazy var exchange: Exchange = {
        var exchange = Exchange()
        exchange.provider = self.dataProvider
        return exchange
    }()
    
    lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter
    }()
    
    var fromScroller: InfScrollerView? {
        didSet {
            fromScroller?.user = user
            fromScroller?.interactor = self
        }
    }
    var toScroller: InfScrollerView? {
        didSet {
            toScroller?.user = user
            toScroller?.interactor = self
        }
    }
    
    func format(_ value: Double, currency: Currency) -> String? {
        formatter.currencySymbol = currency.symbol
        return formatter.string(for: value)
    }
    
    func reloadData(_ allow: Bool? = true) {
        
        transferButton.isEnabled = allow == true
        
        guard let from = fromScroller?.activeAccount?.currency,
              let to = toScroller?.activeAccount?.currency,
              let rate = dataProvider?.rate(from: from, to: to) else {
              
            return
        }
        
        fromScroller?.updateRateLabel("")
        toScroller?.updateRateLabel("\(format(1.0, currency: from) ?? "Err") = \(format(rate, currency: to) ?? "Err")")
        
        if let fromAmount = fromScroller?.activeAmount {
            let string = formatter.string(for: fromAmount * rate) ?? ""
            toScroller?.updateAmountLabel(string)
        }
        
        if let toAccount = toScroller?.activeAccount {
            toScroller?.updateAccountLabel(toAccount.description)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        user.exchanger = exchange
        
        dataProvider?.fetchRates() { rates, error in
//            self.user.exch(from: .eur, to: .usd, amount: 1.0)
//            self.user.exch(from: .gbp, to: .usd, amount: 101.0)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let identifier = segue.identifier else { return }
        switch identifier {
        case "FromScrollerSegue":
            if let scroller = segue.destinationViewController as? InfScrollerView {
                fromScroller = scroller
            }
        case "ToScrollerSegue":
            if let scroller = segue.destinationViewController as? InfScrollerView {
                toScroller = scroller
                toScroller?.isReadonly = true
            }
        default:
            break
        }
    }
    
    @IBOutlet weak var transferButton: UIButton!
    @IBAction func onTransferPressed(_ sender: AnyObject)
    {
        guard let from = fromScroller?.activeAccount?.currency,
              let to = toScroller?.activeAccount?.currency,
              let amount = fromScroller?.activeAmount else { return }

        if !self.user.exch(from: from, to: to, amount: amount) {
            print("Fail to exchange")
        }
        self.reloadData()
    }

}

extension ViewController: Interactor {
    
    func onAmountChanged(amount: Double, allow: Bool?) {
        self.reloadData(allow)
    }
    
    func onAccountChanged(account: Account) {
        self.reloadData()
    }
    
}
