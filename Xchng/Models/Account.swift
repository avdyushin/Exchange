//
//  User.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 06.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import Foundation

class Account {
    
    enum Transactions {
        case income(Double), outcome(Double)
    }
    
    var currency: Currency
    var amount: Double = 0
    
    lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = self.currency.symbol
        return formatter
    }()
    
    init(_ currency: Currency, amount: Double = 0) {
        self.currency = currency
        self.amount = amount
    }
    
    func doTransaction(_ transaction: Transactions) {
        switch transaction {
        case .income(let amount):
            self.amount += amount
        case .outcome(let amount):
            self.amount -= amount
        }
        updated?()
    }
    
    var updated: (() -> Void)?
    
}

func ==(lhs: Account, rhs: Account) -> Bool {
    return lhs.currency == rhs.currency
}

extension Account: Hashable {
    var hashValue: Int { return currency.hashValue }
}

extension Account: CustomStringConvertible {
    var description: String {
        let value = self.formatter.string(for: amount) ?? NSLocalizedString("Error", comment: "Error in amount formatter")
        return NSLocalizedString("You have \(value)", comment: "Account description")
    }
}
