//
//  User.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 06.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import Foundation

class User {
    
    lazy var accounts: Set<Account> = {
        
        var accounts = Set<Account>()
        accounts.insert(Account(.rub, amount: 100.0))
        accounts.insert(Account(.eur, amount: 100.0))
        accounts.insert(Account(.usd, amount: 100.0))
        accounts.insert(Account(.gbp, amount: 100.0))
        return accounts
        
    }()
    
    func account(_ currency: Currency) -> Account? {
        return accounts.filter { $0.currency == currency }.first
    }
    
    var exchanger: Exchange?
    
    func exch(from: Currency, to: Currency, amount: Double) -> Bool {
        do {
            let result = try exchanger?.exchange(from: account(from), to: account(to), amount: amount)
            return result == true
        } catch {
            print("Exchange error: \(error)")
            return false
        }
    }
    
}
