//
//  Currency.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 06.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import Foundation

enum Currency {
    
    case eur, usd, gbp, rub
    
    init?(_ string: String) {
        switch string.uppercased() {
        case "EUR": self = .eur
        case "USD": self = .usd
        case "GBP": self = .gbp
        case "RUB": self = .rub
        default: return nil
        }
    }
    
}

extension Currency {
    
    var code: String {
        switch self {
        case .eur: return "EUR"
        case .usd: return "USD"
        case .gbp: return "GBP"
        case .rub: return "RUB"
        }
    }
    
    var symbol: String {
        switch self {
        case .eur: return "€"
        case .usd: return "$"
        case .gbp: return "£"
        case .rub: return "₽"
        }
    }
    
}
