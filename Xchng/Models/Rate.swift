//
//  Rate.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 06.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import Foundation

struct Rate {
 
    let currency: Currency
    let rate: Double
    
    init?(_ currency: String, rate: Double) {
        guard let currency = Currency(currency) else { return nil }
        self.currency = currency
        self.rate = rate
    }
    
}
