//
//  Ecb.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 06.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import Foundation

enum EcbError: ErrorProtocol {
    case fetchFail, parseFail
}

class Ecb: NSObject {
    
    var queue = DispatchQueue(label: "ru.avdyushin.parser", attributes: DispatchQueueAttributes.serial)
    var rates = [Rate]()
    
    func dataURL(isLocal local: Bool = true) -> URL? {
        if local {
            guard let string = Bundle.main.pathForResource("eurofxref-daily", ofType: "xml") else {
                return nil
            }
            return URL(fileURLWithPath: string)
        } else {
            let string = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
            return URL(string: string)
        }
    }
    
    func fetchData(completion: ((rates: [Rate]?, error: ErrorProtocol?) -> Void)?) {
        
        queue.async { [unowned self] in
            do {
                
                guard let url = self.dataURL(isLocal: false) else {
                    throw EcbError.fetchFail
                }
            
                let data = try Data(contentsOf: url)
                
                let xml = XMLParser(data: data)
                xml.delegate = self
                xml.parse()
                
                guard xml.parserError == nil else {
                    throw EcbError.parseFail
                }
                
                // Base EUR rate
                if let eurRate = Rate("EUR", rate: 1.0) {
                    self.rates.append(eurRate)
                }
                
                completion?(rates: self.rates, error: nil)
                
            } catch {
                completion?(rates: nil, error: error)
            }
        }

    }
    
}

extension Ecb: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "Cube",
            let currency = attributeDict["currency"],
            let rateString = attributeDict["rate"],
            let rateDouble = Double(rateString),
            let rate = Rate(currency, rate: rateDouble) {
            
            self.rates.append(rate)
        }
    }
    
}

extension Ecb: DataProvider {
    
    func fetchRates(completion: ((rates: [Rate]?, error: ErrorProtocol?) -> Void)?) {
        self.fetchData(completion: completion)
    }
    
    func rate(from: Currency, to: Currency) -> Double? {
        
        let fromRate = self.rates.filter { $0.currency == from }.first
        let toRate = self.rates.filter{ $0.currency == to }.first
        
        guard let fromRateValue = fromRate?.rate, let toRateValue = toRate?.rate
        where fromRateValue > 0 && toRateValue > 0 else {
            return nil
        }
        
        return toRateValue / fromRateValue
    }
    
}
