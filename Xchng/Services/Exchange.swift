//
//  Exchange.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 07.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import Foundation

enum ExchangeErrors: ErrorProtocol {
    case noAccount(String)
    case noMoney(String)
    case noRates(String)
}

struct Exchange {
    
    var provider: DataProvider?
    func exchange(from: Account?, to: Account?, amount: Double) throws -> Bool {
        guard let fromAccount = from else {
            throw ExchangeErrors.noAccount("No account \(from)")
        }
        guard let toAccount = to else {
            throw ExchangeErrors.noAccount("No account \(to)")
        }
        guard let rate = provider?.rate(from: fromAccount.currency, to: toAccount.currency) else {
            throw ExchangeErrors.noRates("No exchange rates from \(from) to \(to)")
        }
        guard fromAccount.amount >= amount else {
            throw ExchangeErrors.noMoney("No enough money at \(from), needs \(amount)")
        }
        fromAccount.doTransaction(Account.Transactions.outcome(amount))
        toAccount.doTransaction(Account.Transactions.income(amount * rate))
        return true
    }
    
}
