//
//  DataProvider.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 06.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import Foundation

protocol DataProvider {
    
    func fetchRates(completion: ((rates: [Rate]?, error: ErrorProtocol?) -> Void)?)
    func rate(from: Currency, to: Currency) -> Double?
    
}
