//
//  ScrollerView.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 07.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import UIKit

class InfScrollerView: UIPageViewController {
    
    var interactor: Interactor?
    
    var user: User? {
        didSet {
            bindAccounts()
        }
    }
    
    func bindAccounts() {
        viewAt(index: 0).account = user?.account(.eur)
        viewAt(index: 1).account = user?.account(.usd)
        viewAt(index: 2).account = user?.account(.gbp)
        viewAt(index: 3).account = user?.account(.rub)
    }

    var isReadonly: Bool = false {
        didSet {
            views.forEach {
                $0.isReadonly = isReadonly
            }
        }
    }
    
    func updateOtherAmounts(_ amount: String?, excludeView: AccountView) {
        views.filter{ $0 != excludeView }.forEach {
            $0.amount = amount
        }
    }
    
    func updateAmountLabel(_ amount: String) {
        guard let vc = self.viewControllers?[0] as? AccountView else { return }
        vc.textField?.text = amount
    }
    
    func updateRateLabel(_ rate: String) {
        guard let vc = self.viewControllers?[0] as? AccountView else { return }
        vc.rateLabel?.text = rate
    }
    
    func updateAccountLabel(_ account: String) {
        guard let vc = self.viewControllers?[0] as? AccountView else { return }
        vc.accountLabel?.text = account
    }
    
    lazy var views: [AccountView] = {
        var items = [AccountView]()
        for i in 0..<4 {
            let view = AccountView(nibName: "AccountView", bundle: Bundle.main)
            view.viewIndex = i
            view.isReadonly = self.isReadonly
            view.onValueChanged = { [weak self] value, allow in
                self?.interactor?.onAmountChanged(amount: value, allow: allow)
                self?.updateOtherAmounts(view.textField?.text, excludeView: view)
            }
            items.append(view)
        }
        return items
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        self.setViewControllers([viewAt(index: 0)], direction: .forward, animated: false) { _ in }
    }
    
    override func disablesAutomaticKeyboardDismissal() -> Bool {
        return true
    }
    
    func viewAt(index: Int) -> AccountView {
        var index = index
        if index < 0 { index = views.count - 1 }
        if index > views.count - 1 { index = 0 }
        return views[index]
    }
    
    var activeAccount: Account? {
        guard let vc = self.viewControllers?[0] as? AccountView,
              let account = vc.account else { return nil }
        
        return account
    }
    
    var activeAmount: Double? {
        guard let vc = self.viewControllers?[0] as? AccountView,
              let text = vc.textField?.text,
              let amount = Double(text) else { return nil }
        
        return amount
    }
    
}

extension InfScrollerView: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? AccountView else {
            return nil
        }
        return self.viewAt(index: vc.viewIndex + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? AccountView else {
            return nil
        }
        return self.viewAt(index: vc.viewIndex + 1)
    }
    
}

extension InfScrollerView: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let vc = pageViewController.viewControllers?[0] as? AccountView,
            let account = vc.account {
            
            self.interactor?.onAccountChanged(account: account)
        }
    }
    
}
