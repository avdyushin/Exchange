//
//  AccountView.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 07.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import UIKit

class AccountView: UIViewController {

    var viewIndex = 0
    var amount: String? = nil {
        didSet {
            textField?.text = amount
        }
    }
    var account: Account? {
        didSet {
            reloadData()
            account?.updated = { [weak self] in
                self?.reloadData()
            }
        }
    }
    
    func reloadData() {
        currencyLabel?.text = account?.currency.code
        accountLabel?.text = account?.description
    }
    
    var isReadonly: Bool = false {
        didSet {
            textField?.isEnabled = !isReadonly
        }
    }
    
    var onValueChanged: ((value: Double, allow: Bool?) -> Void)?
    
    @IBOutlet weak var currencyLabel: UILabel?
    @IBOutlet weak var accountLabel: UILabel?
    @IBOutlet weak var rateLabel: UILabel? {
        didSet {
            rateLabel?.text = ""
        }
    }
    @IBOutlet weak var textField: UITextField? {
        didSet {
            textField?.text = amount
            textField?.isEnabled = !isReadonly
            textField?.addTarget(self, action: #selector(self.valueChanged(sender:)), for: .editingChanged)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func disablesAutomaticKeyboardDismissal() -> Bool {
        return true
    }
    
    func valueChanged(sender: UITextField) {
        if let text = sender.text, let value = Double(text) {
            
            let allow = value <= account?.amount
            if !allow {
                self.accountLabel?.textColor = UIColor.red()
            } else {
                self.accountLabel?.textColor = UIColor.black()
            }
            
            onValueChanged?(value: value, allow: allow)
            
        }
    }

}
