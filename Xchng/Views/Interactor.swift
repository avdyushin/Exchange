//
//  Interactor.swift
//  Xchng
//
//  Created by Grigory Avdyushin on 07.07.16.
//  Copyright © 2016 Grigory Avdyushin. All rights reserved.
//

import UIKit

protocol Interactor {

    func onAmountChanged(amount: Double, allow: Bool?)
    func onAccountChanged(account: Account)
    
}
